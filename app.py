import json
import os
from pathlib import Path
from datetime import datetime, timedelta
from urllib.parse import urlparse

import dns.resolver
import flask as fl
from celery import Celery

from whohostwho import DataAcquisition


def make_celery(app):
    celery = Celery(
        app.import_name,
        backend=app.config["result_backend"],
        broker=app.config["CELERY_BROKER_URL"],
    )
    celery.conf.update(app.config)

    class ContextTask(celery.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery.Task = ContextTask
    return celery


redis = os.environ.get("REDIS_URI", "redis://localhost:6379")
app = fl.Flask(__name__)
app.config.update(CELERY_BROKER_URL=redis, result_backend=redis)

celery = make_celery(app)


def get_path(domain):
    return Path("./cache/{}.json".format(domain.replace(".", "_")))


@celery.task()
def resolve_domain(domain):
    print("runnning for domain", domain)
    da = DataAcquisition(ns=("9.9.9.9", "80.67.169.12", "80.67.169.40"))
    cache = get_path(domain)
    print("cahce file", cache)
    try:
        data = da.run(domain)
        data["meta"]["status"] = "resolved"
    except dns.resolver.NXDOMAIN:
        data = {"meta": {"status": "notfound"}}
    print("result", data)
    cache.write_text(json.dumps(data))


def can_refresh(domain):
    cache = get_path(domain)
    if not cache.exists():
        return True
    data = json.loads(cache.read_text())
    date_cache = data.get("meta", {}).get("created_at", None)
    if date_cache is None:
        return False
    date_cache = datetime.strptime(date_cache, "%Y-%m-%d %H:%M:%S")
    if datetime.utcnow() - date_cache > timedelta(hours=1):
        return True
    return False


def get_data(domain):
    cache = get_path(domain)
    if cache.exists():
        data = json.loads(cache.read_text())
    else:
        resolve_domain.delay(domain)
        data = {"meta": {"status": "queue"}}
        cache.write_text(json.dumps(data))
    return data


@app.errorhandler(404)
def page_not_found(error):
    if "json" in fl.request.headers.get("accept", ""):
        data = {"meta": {"status": "notfound"}}
        return fl.jsonify(**data), 404
    return fl.render_template("404.html"), 404


@app.route("/")
def index():
    domain = fl.request.args.get("d", None)
    if domain is not None:
        if "://" not in domain:
            domain = "http://" + domain
        parsed = urlparse(domain)
        return fl.redirect(fl.url_for("domain", domain=parsed.hostname))
    return fl.render_template("index.html", host=fl.request.host)


@app.route("/r/<domain>")
def refresh(domain):
    if not domain.endswith("."):
        domain += "."
    if not can_refresh(domain):
        return fl.redirect(fl.url_for("domain", domain=domain))
    cache = get_path(domain)
    if cache.exists() and cache.is_file():
        cache.unlink()
    return fl.redirect(fl.url_for("domain", domain=domain))


@app.route("/d/<domain>")
def domain(domain):
    if not domain.endswith("."):
        domain += "."
    data = get_data(domain)
    if data.get("meta", {}).get("status", "notfound") == "notfound":
        fl.abort(404)
    data["domain"] = domain
    if "json" in fl.request.headers.get("accept", ""):
        return fl.jsonify(**data)
    else:
        data["refresh"] = can_refresh(domain)
        return fl.render_template("domain.html", data=data)
